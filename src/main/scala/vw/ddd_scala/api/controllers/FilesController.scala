/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.api.controllers

import cats.effect._
import doobie.implicits._
import doobie.util.transactor.Transactor
import sttp.capabilities.WebSockets
import sttp.capabilities.fs2.Fs2Streams
import sttp.model.StatusCode
import sttp.tapir._
import sttp.tapir.codec.refined._
import sttp.tapir.server.ServerEndpoint
import vw.ddd_scala.core.domain.entities._
import vw.ddd_scala.core.primaryAdapters._

final class FilesController(_filesService: FilesService, _xa: Transactor[IO])(implicit blocker: Blocker, contextShift: ContextShift[IO], sf: Sync[IO]) {
  val uploadFile =
    FilesController.upload.serverLogic(x => uploadFn(x._1, x._2))

  def uploadFn(token: AuthToken, stream: fs2.Stream[IO, Byte]): IO[Either[String, String]] =
    _filesService
      .upload[IO](token, stream)
      .value
      .foldMap(vw.ddd_scala.core.secondaryAdapters.createInterpreter)
      .transact(_xa)
      .map(x => x.map(y => y.toString))
  val endpoints = List(
    uploadFile
  )
}

object FilesController {
  def upload =
    endpoint.post
      .in("api")
      .in("v1")
      .in("files")
      .tag("Files")
      .in("upload")
      .in(auth.bearer[AuthToken]())
      .summary("Загрузить файл картинки на сервер")
      .description("Загружает выбранную картинку")
      .in(streamBinaryBody(Fs2Streams[IO]))
      .errorOut(stringBody)
      .out(stringBody)
}
