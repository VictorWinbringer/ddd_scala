/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala

import java.util.concurrent.{ ExecutorService, Executors }
import cats.effect._
import cats.implicits._
import com.typesafe.config._
import doobie.Transactor
import doobie.util.ExecutionContexts
import vw.ddd_scala.api._
import vw.ddd_scala.config._
import vw.ddd_scala.db.FlywayDatabaseMigrator
import eu.timepit.refined.auto._
import org.http4s.ember.server._
import org.http4s.implicits._
import org.http4s.server.Router
import org.slf4j.LoggerFactory
import pureconfig._
import sttp.tapir.docs.openapi._
import sttp.tapir.openapi.circe.yaml._
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.http4s.SwaggerHttp4s
import vw.ddd_scala.api.controllers.FilesController
import vw.ddd_scala.core.domain.entities.AuthToken
import vw.ddd_scala.core.domain.services.{ FilesRepository, MyApp, TokenParser, UsersRepository, UuidsRepository }
import vw.ddd_scala.core.primaryAdapters.FilesService
import scala.concurrent.ExecutionContext

object Server extends IOApp.WithContext {
  val blockingPool: ExecutorService                                                   = Executors.newCachedThreadPool()
  val ec: ExecutionContext                                                            = ExecutionContext.fromExecutor(blockingPool)
  val log                                                                             = LoggerFactory.getLogger(Server.getClass())
  override protected def executionContextResource: Resource[SyncIO, ExecutionContext] = Resource.eval(SyncIO(ec))

  override def run(args: List[String]): IO[ExitCode] = {
    implicit val blocker = Blocker.liftExecutorService(blockingPool)
    val migrator         = new FlywayDatabaseMigrator
    val config           = ConfigFactory.load(getClass().getClassLoader())
    val dbConfig         = ConfigSource.fromConfig(config).at(DatabaseConfig.CONFIG_KEY).loadOrThrow[DatabaseConfig]
    val serviceConfig    = ConfigSource.fromConfig(config).at(ServiceConfig.CONFIG_KEY).loadOrThrow[ServiceConfig]
    val tr = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver",
      dbConfig.url,
      dbConfig.user,
      dbConfig.pass,
      blocker
    )
    val filesController = new FilesController(
      new FilesService(
        new UsersRepository[MyApp](),
        new FilesRepository[MyApp](),
        new UuidsRepository[MyApp](),
        new TokenParser[MyApp]()
      ),
      tr
    )
    val endpoints = filesController.endpoints
    val docs      = OpenAPIDocsInterpreter().serverEndpointsToOpenAPI(endpoints, "ddd_scala", "1.0.0")
    val swagger   = new SwaggerHttp4s(docs.toYaml)
    val routes =
      swagger.routes[IO] <+> Http4sServerInterpreter[IO].toRoutes(serverEndpoints = filesController.endpoints)
    val httpApp = Router("/" -> routes).orNotFound
    val resource = EmberServerBuilder
      .default[IO]
      .withBlocker(blocker)
      .withHost(serviceConfig.ip)
      .withPort(serviceConfig.port)
      .withHttpApp(httpApp)
      .build

    resource
      .use(server =>
        for {
          _  <- IO.delay(log.info("Server started at {}", server.address))
          mr <- migrator.migrate(dbConfig.url, dbConfig.user, dbConfig.pass)
          exitCode <-
            if (!mr.success) IO.delay(log.info("DB migration filed")) >> IO.delay(ExitCode.Error)
            else IO.never.as(ExitCode.Success)
        } yield exitCode
      )
      .attempt
      .map {
        case Left(e) =>
          log.error("An error occured during execution!", e)
          ExitCode.Error
        case Right(s) => s
      }
  }

}
