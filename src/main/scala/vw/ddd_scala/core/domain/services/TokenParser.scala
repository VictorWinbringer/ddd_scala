/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain.services

import cats.InjectK
import cats.free.Free
import vw.ddd_scala.core.domain.entities.{AuthToken, UserId}

sealed trait TokenParserAlgebra[A]

case class Parse(token: AuthToken) extends TokenParserAlgebra[Either[String, UserId]]

class TokenParser[F[_]](implicit I: InjectK[TokenParserAlgebra, F]) {
  def parse(token: AuthToken): Free[F, Either[String, UserId]] = Free.liftInject[F](Parse(token))
}

object TokenParser {
  implicit def tokenParser[F[_]](implicit I: InjectK[TokenParserAlgebra, F]): TokenParser[F] = new TokenParser[F]
}