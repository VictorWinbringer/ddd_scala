/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain.services

import cats.InjectK
import cats.effect.{IO, Sync}
import cats.free.Free

import java.util.UUID

sealed trait UuidsRepositoryAlgebra[A]

case class GetNext() extends UuidsRepositoryAlgebra[UUID]

class UuidsRepository[F[_]](implicit I: InjectK[UuidsRepositoryAlgebra, F]) {
  def getNext(): Free[F, UUID] = Free.liftInject[F](GetNext())
}

object UuidsRepository {
  implicit def uuidsRepository[F[_]](implicit I: InjectK[UuidsRepositoryAlgebra, F]): UuidsRepository[F] = new UuidsRepository[F]
}


//typeclass way ----------------------------------------------------

//domain ----------------------------------------------------------
trait UuidRepositoryAlgebra[F[_], A] {
  def create(a: A): F[UUID]
}

object UuidRepositoryAlgebra {
  def apply[F[_], A](implicit algebra: UuidRepositoryAlgebra[F, A]): UuidRepositoryAlgebra[F, A] = algebra

  implicit class UuidRepositoryOps[F[_], A](a: A)(implicit algebra: UuidRepositoryAlgebra[F, A]) {
    def create() = algebra.create(a)
  }

}

//primary adapters -----------------------------------------------

class UuidService[F[_], A](_uuidRepository: A)(implicit _uuidRepositoryAlgebra: UuidRepositoryAlgebra[F, A]) {

  import UuidRepositoryAlgebra._

  def create() = _uuidRepository.create()
}

//secondary adapters -------------------------------------------------

final class UuidRepositoryInterpreter[F[_] : Sync, A] extends UuidRepositoryAlgebra[F, A] {
  def create(a: A): F[UUID] = Sync[F].delay(UUID.randomUUID())
}

//Usage -----------------------------------

case class UuidRepository()

class TypeclassWayExample {

  def create() = {

    implicit val interpreter = new UuidRepositoryInterpreter[IO, UuidRepository]
    val repo = new UuidRepository()
    val service = new UuidService[IO, UuidRepository](repo)
    service.create().map(x => println(x))
  }
}
