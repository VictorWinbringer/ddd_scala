/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain

import cats.data.EitherK

package object services {
  type m1[A] = EitherK[UsersRepositoryAlgebra, FilesRepositoryAlgebra, A]
  type m2[A] = EitherK[UuidsRepositoryAlgebra, m1, A]
  type MyApp[A] = EitherK[TokenParserAlgebra, m2, A]
}
