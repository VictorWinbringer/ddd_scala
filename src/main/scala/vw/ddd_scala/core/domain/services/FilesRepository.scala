/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain.services

import cats.InjectK
import cats.free.Free
import vw.ddd_scala.core.domain.entities.UserFile

import java.util.UUID

sealed trait FilesRepositoryAlgebra[A]

case class Add[F[_]](file: UserFile[F], id: UUID) extends FilesRepositoryAlgebra[Unit]

class FilesRepository[F[_]](implicit I: InjectK[FilesRepositoryAlgebra, F]) {
  def add[S[_]](file: UserFile[S], id: UUID): Free[F, Unit] = Free.liftInject[F](Add(file, id))
}

object FilesRepository {
  implicit def filesRepository[F[_]](implicit I: InjectK[FilesRepositoryAlgebra, F]): FilesRepository[F] = new FilesRepository[F]
}