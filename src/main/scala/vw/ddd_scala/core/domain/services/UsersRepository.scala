/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain.services

import cats._
import cats.free._
import vw.ddd_scala.core.domain.entities._

import scala.util.Either

sealed trait UsersRepositoryAlgebra[A]

case class Get(id: UserId) extends UsersRepositoryAlgebra[Either[String, User]]

class UsersRepository[F[_]](implicit I: InjectK[UsersRepositoryAlgebra, F]) {
  def get(id: UserId): Free[F, Either[String, User]] = Free.liftInject[F](Get(id))
}

object UsersRepository {
  implicit def usersRepository[F[_]](implicit I: InjectK[UsersRepositoryAlgebra, F]): UsersRepository[F] = new UsersRepository[F]
}








