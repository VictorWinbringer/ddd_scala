/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain.entities

final class UserRolesOwner extends RolesOwner[User] {
  override def isInRole(a: User, role: String): Boolean = a.roles.exists(x => x.value.toLowerCase == role.toLowerCase)
}
