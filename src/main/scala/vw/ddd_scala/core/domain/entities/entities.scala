/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.domain

import eu.timepit.refined.api.{ Refined, RefinedTypeOps }
import eu.timepit.refined.cats.CatsRefinedTypeOpsSyntax
import eu.timepit.refined.collection.NonEmpty
import eu.timepit.refined.numeric.Interval
import eu.timepit.refined.types.all.NonEmptyFiniteString

package object entities {
  type AuthToken = String Refined NonEmpty

  object AuthToken extends RefinedTypeOps[AuthToken, String] with CatsRefinedTypeOpsSyntax

  type UserId = String Refined NonEmpty

  object UserId extends RefinedTypeOps[UserId, String] with CatsRefinedTypeOpsSyntax

  type UserRole = NonEmptyFiniteString[255]
  object UserRole extends RefinedTypeOps[UserRole, String] with CatsRefinedTypeOpsSyntax

  type UserAge = Int Refined Interval.ClosedOpen[18, 150]

  object UserAge extends RefinedTypeOps[UserAge, Int] with CatsRefinedTypeOpsSyntax

  type UserFile[F[_]] = fs2.Stream[F, Byte]

  implicit val userRolesOwner = new UserRolesOwner()

  implicit class RolesOwnerOps[A](a: A)(implicit t: RolesOwner[A]) {
    def isInRole(role: String): Boolean = t.isInRole(a, role)
  }
}
