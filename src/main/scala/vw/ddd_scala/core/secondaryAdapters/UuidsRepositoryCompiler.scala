/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.secondaryAdapters

import cats._
import doobie._
import doobie.implicits._
import vw.ddd_scala.core.domain.services._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import com.nimbusds.jose._
import com.nimbusds.jose.util._
import com.nimbusds.jwt.JWTParser
import com.nimbusds.oauth2.sdk.id._
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator
import doobie.util.query.Query0

import java.util.UUID
import fs2.io.file._
import vw.ddd_scala.core.domain.entities._

import java.net.URL
import java.nio.file.{ Paths, StandardOpenOption }
import scala.util._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

final class UuidRepositoryCompiler extends (UuidsRepositoryAlgebra ~> MyIo) {
  def apply[A](fa: UuidsRepositoryAlgebra[A]): MyIo[A] =
    fa match {
      case GetNext() => IO.delay(UUID.randomUUID().asInstanceOf[A]).to[ConnectionIO]
    }
}






