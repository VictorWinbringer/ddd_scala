package vw.ddd_scala.core.secondaryAdapters

import cats._
import doobie._
import doobie.implicits._
import vw.ddd_scala.core.domain.services._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import com.nimbusds.jose._
import com.nimbusds.jose.util._
import com.nimbusds.jwt.JWTParser
import com.nimbusds.oauth2.sdk.id._
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator
import doobie.util.query.Query0

import java.util.UUID
import fs2.io.file._
import vw.ddd_scala.core.domain.entities._

import java.net.URL
import java.nio.file.{ Paths, StandardOpenOption }
import scala.util._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

final class TokenParserCompiler extends (TokenParserAlgebra ~> MyIo) {
  override def apply[A](fa: TokenParserAlgebra[A]): MyIo[A] =
    fa match {
      case Parse(token: AuthToken) => parse(token).map(_.asInstanceOf[A]): MyIo[A]
    }

  def parse(token: AuthToken): MyIo[Either[String, UserId]] = {
    val res = Try[String] {
      //Адрес SSO сервера
      val iss = new Issuer("https://dev-t-ca3k92.us.auth0.com/")
      //Идентификатор нашего клиента
      val clientID = new ClientID("http://127.0.0.1:8888")
      val jwsAlg   = JWSAlgorithm.RS256
      //Адрес по которому загружать данные для JWK
      val jwkSetURL = new URL("https://dev-t-ca3k92.us.auth0.com/.well-known/jwks.json")
      val validator = new IDTokenValidator(iss, clientID, jwsAlg, jwkSetURL, new DefaultResourceRetriever())
      val idToken   = JWTParser.parse(token.value)
      //Валидируем токен и получаем сохраненные в нем данные.
      //Идет проверка времени жизни и других параметров
      val claims = validator.validate(idToken, null)
      claims.getSubject().getValue
    }.toEither.swap
      .map(x => x.getMessage)
      .swap
      .flatMap(x => UserId.from(x))
    IO.delay(res).to[ConnectionIO]
  }
}
