package vw.ddd_scala.core.secondaryAdapters

import cats._
import doobie._
import doobie.implicits._
import vw.ddd_scala.core.domain.services._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import com.nimbusds.jose._
import com.nimbusds.jose.util._
import com.nimbusds.jwt.JWTParser
import com.nimbusds.oauth2.sdk.id._
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator
import doobie.util.query.Query0

import java.util.UUID
import fs2.io.file._
import vw.ddd_scala.core.domain.entities._

import java.net.URL
import java.nio.file.{ Paths, StandardOpenOption }
import scala.util._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

final class FilesRepositoryCompiler(implicit
    val blocker: Blocker,
    contextShift: ContextShift[IO],
    sf: Sync[IO]
) extends (FilesRepositoryAlgebra ~> MyIo) {
  def apply[A](fa: FilesRepositoryAlgebra[A]): MyIo[A] =
    fa match {
      case x: Add[IO] =>
        x.file
          .through(
            fs2.io.file.writeAll[IO](
              Paths.get(s"images/${x.id}.file"),
              blocker,
              List(StandardOpenOption.CREATE, StandardOpenOption.WRITE)
            )
          )
          .compile
          .drain
          .to[ConnectionIO]
          .map(x => x.asInstanceOf[A])
    }
}
