package vw.ddd_scala.core.secondaryAdapters

import cats._
import doobie._
import doobie.implicits._
import vw.ddd_scala.core.domain.services._
import cats.implicits._
import cats.effect._
import cats.effect.implicits._
import com.nimbusds.jose._
import com.nimbusds.jose.util._
import com.nimbusds.jwt.JWTParser
import com.nimbusds.oauth2.sdk.id._
import com.nimbusds.openid.connect.sdk.validators.IDTokenValidator
import doobie.util.query.Query0

import java.util.UUID
import fs2.io.file._
import vw.ddd_scala.core.domain.entities._

import java.net.URL
import java.nio.file.{ Paths, StandardOpenOption }
import scala.util._
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

final class UsersRepositoryCompiler extends (UsersRepositoryAlgebra ~> MyIo) {
  override def apply[A](fa: UsersRepositoryAlgebra[A]): MyIo[A] =
    fa match {
      case vw.ddd_scala.core.domain.services.Get(id) => get(id).map(_.asInstanceOf[A])
    }

  def get(id: UserId): MyIo[Either[String, User]] = select(id).map(_.toRight[String]("User not found")).map(parse)

  def parse(input: Either[String, (String, String, Int)]): Either[String, User] =
    for {
      t         <- input
      id        <- UserId.from(t._1)
      rolesList <- decode[List[String]](t._2).swap.map(_.getMessage).swap
      roles     <- toRoleList(rolesList)
      age       <- UserAge.from(t._3)
    } yield User(id, age, roles)

  def toRoleList(rolesList: List[String]): Either[String, List[UserRole]] =
    rolesList
      .map(x => UserRole.from(x).map(y => List(y)))
      .fold((List(): List[UserRole]).asRight[String])(concat)

  def concat(a: Either[String, List[UserRole]], b: Either[String, List[UserRole]]) =
    for {
      ar <- a
      br <- b
    } yield ar.combine(br)

  def select(id: UserId) = sql"""
         SELECT "Id", "Roles", "Age"
         FROM "Users"
         WHERE "Id" = ${id.value}
         """.query[(String, String, Int)].option
}
