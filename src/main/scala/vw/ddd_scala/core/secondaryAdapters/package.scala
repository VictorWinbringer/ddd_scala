/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core
import cats.data._
import cats.free._
import cats._
import cats.effect.{ Blocker, ContextShift, IO, Sync }
import doobie.ConnectionIO
import vw.ddd_scala.core.domain.services._

package object secondaryAdapters {
  type MyIo[A] = ConnectionIO[A]
  def createInterpreter(implicit blocker: Blocker, contextShift: ContextShift[IO], sf: Sync[IO]):(MyApp ~> MyIo) = {
    val i1: m1 ~> MyIo             = new UsersRepositoryCompiler or new FilesRepositoryCompiler
    val i2: m2 ~> MyIo             = new UuidRepositoryCompiler or i1
    val interpreter: MyApp ~> MyIo = new TokenParserCompiler or i2
    interpreter
  }
}
