/*
 * Copyright (c) 2022 vw
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package vw.ddd_scala.core.primaryAdapters

import cats.data.EitherT
import cats.free._
import vw.ddd_scala.core.domain.entities._
import vw.ddd_scala.core.domain.services._
import cats.implicits._
import java.util.UUID

final class FilesService(
    _users: UsersRepository[MyApp],
    _files: FilesRepository[MyApp],
    _uuids: UuidsRepository[MyApp],
    _tokenParser: TokenParser[MyApp]
) {

  type FM[A] = Free[MyApp, A]

  def upload[S[_]](token: AuthToken, file: UserFile[S]): EitherT[FM, String, UUID] =
    for {
      id   <- EitherT(_tokenParser.parse(token))
      user <- EitherT(_users.get(id))
      isAdmin = user.isInRole("admin")
      res <-
        if (!isAdmin) {
          EitherT.fromEither[FM]("You are not prepare!".asLeft[UUID])
        } else {
          saveFile[S](file)
        }
    } yield res

  def saveFile[S[_]](file: UserFile[S]): EitherT[FM, String, UUID] =
    for {
      uid <- EitherT.right[String](_uuids.getNext())
      _   <- EitherT.right[String](_files.add[S](file, uid))
    } yield uid
}
