CREATE TABLE IF NOT EXISTS public."Users"
(
     "Id" character varying(256) COLLATE pg_catalog."default" NOT NULL,
      "Roles" json,
      "Age" integer NOT NULL,
      CONSTRAINT "PK_Users" PRIMARY KEY ("Id")
);
